# Vision2019

All the python vision code to run on the Raspberry Pi coprocessor for all the vision needs in First Robotics Competition 2019.

## Dependencies

### Deploy

```
pip install -r requirements.txt
```
Actually don't do this. Just upload the file.

### Developing

```
pip install -r requirements-dev.txt
```

### Updating Dependencies

Change appropriate version in `requirements-to-freeze.txt` / `requirements-dev.txt`.


### IMPORTANT TODO: 

IMPLEMENT STUFF FROM CHICKENVISION
https://github.com/team3997/ChickenVision/blob/master/ChickenVision.py

#### Potential Changes:
Use angle difference between two contours to get distance to target. This can also be done the chickenvision way, whichever floats our boat.
